const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const place = require('./app/place');
const category = require('./app/category');
const items = require('./app/office');
const app = express();

const port = 8000;

app.use(cors());

app.use(express.json());

app.use(express.static('public'));

const connection  = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'office'
});

connection.connect((err) => {
    if (err) throw err;
    app.use('/items', items(connection));
    app.use('/category', category(connection));
    app.use('/place', place(connection));
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

});





