const express = require('express');
const  config = require('../config');
const multer = require('multer');
const path = require('path');

const nanoid = require('nanoid');

const router = express.Router();
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {

        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = (db) => {
    // Product index
    router.get('/', (req, res) => {
        db.query('SELECT * FROM `item_list`', function (error, results) {
            if (error) throw error;
            res.send(results);
        });
    });

    // Product create
    router.post('/', upload.single('image'), (req, res) => {

        const item = req.body;

        if (req.file) {
            item.image = req.file.filename;
        } else {
            item.image = null;
        }

        db.query(
            'INSERT INTO `item_list` (`name`, `category_id`, `place_id`, `description`, `image`)' +
            'VALUES(?, ?, ?, ?, ?)',
            [item.name, 1, 1, item.description, item.image],
            (error, results) => {
                if (error) throw error;
                item.id = results.insertId;
            res.send(item);
            }
        );

    });

    router.get('/:id', (req, res) => {
        db.query('SELECT * FROM `item_list` WHERE `id` =' + req.params.id, function(error, results) {
            if (error) throw error;
            res.send(results);
        })

    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `item_list` WHERE `id` = ' + req.params.id, function(error, results) {
            if (error) throw error;
            res.send(results);
        })

    });
    return router;
};

module.exports = createRouter;