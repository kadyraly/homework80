const express = require('express');
const router = express.Router();


const createRouter = (db) => {

    router.get('/', (req, res) => {
        db.query('SELECT * FROM `places`', function (error, results) {
            if (error) throw error;
            res.send(results);
        });
    });


    router.post('/',  (req, res) => {

        const place = req.body;


        db.query(
            'INSERT INTO `places` (`name`,  `description`)' +
            'VALUES(?, ?)',
            [place.name, place.description],
            (error, results) => {
                if (error) throw error;
                place.id = results.insertId;
                res.send(place);
            }
        );

    });

    router.get('/:id', (req, res) => {
        db.query('SELECT * FROM `places` WHERE `id` =' + req.params.id, function(error, results) {
            if (error) throw error;
            res.send(results);
        })

    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `places` WHERE `id` = ' + req.params.id, function(error, results) {
            if (error) throw error;
            res.send(results);
        })

    });
    return router;
};

module.exports = createRouter;