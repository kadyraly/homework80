CREATE DATABASE `office`;
USE `office`;

CREATE TABLE `categories` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `description` TEXT,
    PRIMARY KEY (`id`)
);
CREATE TABLE `places` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `description` TEXT,
    PRIMARY KEY (`id`)
);



CREATE TABLE `item_list`(
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `category_id` INT NOT NULL,
	`place_id` INT NOT NULL,
    `description` TEXT,
    `date` DATETIME,
    `image` VARCHAR(255),
    
    PRIMARY KEY (`id`),
    INDEX `FK_category_idx` (`category_id`),
    CONSTRAINT `FK_category`
		FOREIGN KEY (`category_id`)
        REFERENCES `item_list` (`id`) 
		ON DELETE RESTRICT
        ON UPDATE CASCADE,
	INDEX `FK_place_idx` (`place_id`),
	CONSTRAINT `FK_place`
		FOREIGN KEY (`place_id`)
        REFERENCES `item_list` (`id`) 
		ON DELETE RESTRICT
        ON UPDATE CASCADE
);

INSERT INTO `categories` (`name`, `description`)
VALUES ('comptuter', 'the computer on the table');

INSERT INTO `places` (`name`, `description`)
VALUES ('manager office', 'in the second floor');

INSERT INTO `item_list` (`name`, `category_id`, `place_id`, `description`, `date`, `image`)
VALUES ('item1', 1, 1, 'account the items', '29.05.2018', 'gff.jpg');